#!/bin/bash

EXITCODE=0 # Default exit code

if [[ "$#" == 0 || "$#" -gt 1 ]]; then 
  echo "Usage: crack.sh executable" # give error if wrong num of arguments 
  EXITCODE=1
else 
  if [[ -e "$1" && -r "$1" ]]; then # if readable 
    if [ -x "$1" ]; then  # if executable 
      LOCKBOX=$(readlink -vmf "$1")  # get path 
      for word in $(strings "$LOCKBOX"); # for each string 
        do "$LOCKBOX" "$word" &> /dev/null  # test to see if password 
        TOKEN=$("$LOCKBOX" "$word") # set token if password true 
        if [ "$?" == 0 ]; then 
          echo "Password is "$word""  # print like Bui
          echo "Token    is "$TOKEN""
          exit "$EXITCODE" 
        else 
          EXITCODE=4
        fi 
      done  
    else 
      echo ""$LOCKBOX" is not executable!" # give error if not executable 
      EXITCODE=3
    fi  
  else 
    echo ""$LOCKBOX" is not readable!" # give error if not readable or doesn't exist 
    EXITCODE=2
  fi 
fi 
if [ "$EXITCODE" == 4 ]; then 
  echo "Unable to crack "$LOCKBOX""
fi
exit "$EXITCODE" 

