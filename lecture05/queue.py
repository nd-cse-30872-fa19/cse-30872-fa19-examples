#!/usr/bin/env python3

# queue.py

class Queue(object):
    def __init__(self):
        self.data = []

    def push(self, value):
        self.data.append(value)

    def pop(self):
        return self.data.pop(0)

    def empty(self):
        return len(self.data) == 0

    def __str__(self):
        return 'Queue({})'.format(self.data)

if __name__ == '__main__':
    q = Queue()
    for i in range(5):
        q.push(i)

    print(q)

    while not q.empty():
        print(q.pop())
    
    print(q)
