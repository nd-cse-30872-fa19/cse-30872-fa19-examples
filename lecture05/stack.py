#!/usr/bin/env python3

# stack.py

import collections

Node = collections.namedtuple('Node', 'value next')

class Stack(object):
    def __init__(self):
        self.head = None

    def push(self, value):
        self.head = Node(value, self.head)

    def pop(self):
        node      = self.head
        self.head = self.head.next
        return node.value

    def empty(self):
        return self.head is None

    def __str__(self):
        return 'Stack({})'.format(self.head)

if __name__ == '__main__':
    s = Stack()
    for i in range(5):
        s.push(i)

    print(s)

    while not s.empty():
        print(s.pop())
    
    print(s)
