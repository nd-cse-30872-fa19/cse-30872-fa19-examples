#!/usr/bin/env python3

import itertools
import sys

# Constants

LOTTO_NUMBERS = 6

# Functions

def display_combinations(s, n=LOTTO_NUMBERS):
    for combination in sorted(itertools.combinations(s, n)):
        print(' '.join(map(str, combination)))

def display_combinations(s, n=LOTTO_NUMBERS, c=[], k=0):
    '''
    s:  Numbers to choose from
    n:  Size of combination
    c:  Current combination
    k:  Current element index
    '''
    if k == len(s):
        if len(c) == n:
            print(' '.join(map(str, c)))
        return

    display_combinations(s, n, c + [s[k]], k + 1)
    display_combinations(s, n, c, k + 1)

# Main execution

if __name__ == '__main__':
    for line, numbers in enumerate(map(str.split, sys.stdin)):
        if len(numbers) <= 1:
            break
        
        if line:
            print('')

        display_combinations(list(map(int, numbers[1:])))
