#!/usr/bin/env python3

import sys

# Functions

def read_line(array):
    array[:] = sorted(map(int, sys.stdin.readline().split()), reverse=True)
    return array

def feed_children(children, cookies):
    count = 0

    while cookies and children:
        child  = children.pop(0)
        cookie = cookies[0]

        if child <= cookie:
            cookies.pop(0)
            count += 1

    return count

# Main execution

if __name__ == '__main__':
    children = []
    cookies  = []
    while read_line(children) and read_line(cookies):
        print(feed_children(children, cookies))
