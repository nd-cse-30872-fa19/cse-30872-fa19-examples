#!/usr/bin/env python

import sys

# Functions

def gcd(a, b):
    while b != 0:
        t = b
        b = a % b
        a = t
    return a

def gcd(a, b):
    if b == 0:
        return a

    return gcd(b, a % b)

# Main execution

if __name__ == '__main__':
    for line in sys.stdin:
        a, b = map(int, line.split())
        print('GCD({}, {}) = {}'.format(a, b, gcd(a, b)))
